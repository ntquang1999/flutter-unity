import 'package:flutter/material.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';

class NoInteractionScreen extends StatefulWidget {
  NoInteractionScreen({Key? key}) : super(key: key);

  @override
  _NoInteractionScreenState createState() => _NoInteractionScreenState();
}

class _NoInteractionScreenState extends State<NoInteractionScreen> {
  static final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey<ScaffoldState>();

  late UnityWidgetController _unityWidgetController;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _unityWidgetController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: UnityWidget(
              onUnityCreated: onUnityCreated,
              onUnityMessage: onUnityMessage,
              useAndroidViewSurface: true,
              borderRadius: BorderRadius.all(Radius.circular(70)),
            ),
    );
  }

  void sendMessageToUnity(String message) {
    _unityWidgetController.postMessage(
      'MenuController',
      'MessageReceiver',
       message,
    );
  }

  void onUnityMessage(message) {
    print('Received message from unity: ${message.toString()}');
    if(message.toString() == 'exit')
    {
      Navigator.of(context).pushNamed('/');
    }
  }

  // Callback that connects the created controller to the unity controller
  void onUnityCreated(controller) {
    controller.resume();
    this._unityWidgetController = controller;
    sendMessageToUnity("Hello from Flutter!");
  }
}
